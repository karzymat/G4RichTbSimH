#include "Geant4/globals.hh"
#include <cmath>
#include "RichTbPmtSteppingAction.hh"
#include "RichTbAnalysisManager.hh"
#include "RichTbMaterial.hh"
#include "RichTbGeometryParameters.hh"
#include "RichTbMaterialParameters.hh"
#include "RichTbRunConfig.hh"
#include "RichTbPrimaryGeneratorAction.hh"
#include "Geant4/G4ParticleDefinition.hh"
#include "Geant4/G4DynamicParticle.hh"
#include "Geant4/G4Material.hh"
#include "Geant4/G4Step.hh"
#include "Geant4/G4Track.hh"
#include "Geant4/G4Electron.hh"
#include "Geant4/G4ThreeVector.hh"
#include "Geant4/G4OpticalPhoton.hh"
#include "Geant4/G4PionMinus.hh"
#include "RichTbCounter.hh"
#include "RichTbPhotonUserInfoAttach.hh"
#include "RichTbBeamProperty.hh"
#include "RichTbPhotoElectron.hh"
#include "Geant4/G4Navigator.hh"
#include "Geant4/G4TransportationManager.hh"
#include "RichTbUserTrackInfo.hh"
#include "Geant4/G4ThreeVector.hh"


RichTbPmtSteppingAction::RichTbPmtSteppingAction():mChTrackMinMomFactorForHisto(0.95)
{ }
RichTbPmtSteppingAction::~RichTbPmtSteppingAction()
{
    ;
}

void RichTbPmtSteppingAction::UserSteppingAction(const G4Step * aStep) {



  // RichTbAnalysisManager* aRAnalysisManager = RichTbAnalysisManager::getInstance();
  RichTbRunConfig* aRunConfig =   RichTbRunConfig::getRunConfigInstance();
  G4StepPoint* pPreStepPoint  = aStep ->GetPreStepPoint();
  G4StepPoint* pPostStepPoint = aStep ->GetPostStepPoint();
  const G4ThreeVector prePos= pPreStepPoint->GetPosition();
  const G4ThreeVector postPos= pPostStepPoint->GetPosition();
    RichTbAnalysisManager * myanalysisManager =
           RichTbAnalysisManager::getInstance();


    // G4Track* aTrack = aStep -> GetTrack();
    // const G4DynamicParticle* aParticle = aTrack->GetDynamicParticle();
    // const G4double aTrackEnergy = aParticle->GetKineticEnergy();

    /*
   //check for phelectron
    if( aParticle->GetDefinition() == RichTbPhotoElectron::PhotoElectron()) {
      // G4cout<<" electron! " <<G4endl;
   if( aTrackEnergy > 0.0 )
	    {

	 // check prestep and post are valid physical volume
	      if( pPreStepPoint -> GetPhysicalVolume() &&
		  pPostStepPoint -> GetPhysicalVolume() ) {

		  G4String tpreVol = pPreStepPoint -> GetPhysicalVolume()->GetName();
		  G4String tpostVol = pPostStepPoint -> GetPhysicalVolume()->GetName();

		    G4cout<<" pre post stepvol names "<< tpreVol << "    "<<
		      tpostVol <<G4endl;
	      }
	    }
    }

    */


  // check photon presetp inside the vessel
   if( (prePos.z()< RichTbVesselDnsZEnd ) &&
      (prePos.x()< RichTbVesselXPosExtreme &&
       prePos.x() > RichTbVesselXNegExtreme ) &&
      (prePos.y() <  RichTbVesselYPosExtreme &&
       prePos.y() >  RichTbVesselYNegExtreme) )

     {
       // check photon at a geometrical boundary
       if (pPostStepPoint->GetStepStatus() == fGeomBoundary) {

	  G4Track* aTrack = aStep -> GetTrack();
	  const G4DynamicParticle* aParticle = aTrack->GetDynamicParticle();
	  const G4double aTrackEnergy = aParticle->GetKineticEnergy();

	  // test for photons which are not already killed.

	  if( (aParticle->GetDefinition() == G4OpticalPhoton::OpticalPhoton()) &&
	      (  aTrackEnergy > 0.0 ))
	    {

	 // check prestep and post are valid physical volume
	      if( pPreStepPoint -> GetPhysicalVolume() &&
		  pPostStepPoint -> GetPhysicalVolume() ) {

		  G4String tpreVol = pPreStepPoint -> GetPhysicalVolume()->GetName();
		  G4String tpostVol = pPostStepPoint -> GetPhysicalVolume()->GetName();

      //  G4cout<<" pre post stepvol names "<< tpreVol << "    "<<
		  //    tpostVol <<G4endl;

		  if((tpreVol == CrystalMasterPhysName) &&
         ((tpostVol == PhDetSupPhysNameLeft) || (tpostVol == PhDetSupPhysNameRight)
        		 || (tpostVol == PhDetSupPhysNameBottomLeft)) || (tpostVol == PhDetSupPhysNameBottomRight)) {

		    //print photon coordinate at the current step.
		    // G4cout<<" do something " <<G4endl;
        //		     G4cout<<"Photon incidence on Photonframe  "<< prePos << "  "<<postPos<<G4endl;
		    if( myanalysisManager -> getPhotonXCoord()) {
          // G4cout<<"x coordinate  "<< postPos.x() <<G4endl;
		      myanalysisManager -> getPhotonXCoord()->Fill( postPos.x());
		      // G4cout<<"x coordinate  "<< postPos.x() <<G4endl;
		    }
		    if( myanalysisManager -> getPhotonYCoord()) {
		      myanalysisManager -> getPhotonYCoord()->Fill( postPos.y());
          // G4cout<<"Y coordinate  "<< postPos.y() <<G4endl;
		    }

		    if( myanalysisManager -> getPhotonZCoord()) {
		      myanalysisManager -> getPhotonZCoord()->Fill( postPos.z());
          //  G4cout<<"z coordinate  "<< postPos.z() <<G4endl;
		    }

		    if( myanalysisManager -> getPhotonXYCoord()) {
		      myanalysisManager -> getPhotonXYCoord()->Fill(postPos.x(), postPos.y());
		      //G4cout<<"z coordinate  "<< postPos.z() <<G4endl;
		    }
		    if( myanalysisManager -> getPhotonXYCoordProfile()) {
		      myanalysisManager -> getPhotonXYCoordProfile()->Fill(postPos.y(), postPos.x());
		    }





		      if(postPos.y()>-30.0 && postPos.y()< +30.0){
		       	if((postPos.x()>-60.0 && postPos.x()< -20.0) || (postPos.x()<60.0 && postPos.x()> 20.0)){
		         	double y = postPos.y() - 0.0;
	        		double x = postPos.x() + 0.0;
		        	double radius = sqrt(y*y + x*x);
              double emisptZ= aTrack->GetVertexPosition().z();

		         	myanalysisManager -> getPhotonWidthProfile()->Fill(radius);
             	if(myanalysisManager -> getfCkvRadiusVsEmisPtPhoton())
                 myanalysisManager -> getfCkvRadiusVsEmisPtPhoton()->Fill( emisptZ, radius);
              if(myanalysisManager ->getfEmisPtPhoton())
                myanalysisManager ->getfEmisPtPhoton()->Fill(emisptZ);

               G4VUserTrackInformation* aTkInfo=aTrack->GetUserInformation();
               RichTbUserTrackInfo* curPhotTrackUserInfo= (RichTbUserTrackInfo*)  aTkInfo;
               if(curPhotTrackUserInfo && curPhotTrackUserInfo->HasUserPhotonInfo() ) {
                RichTbPhotonInfo* aRichTbPhotonInfo =
                  curPhotTrackUserInfo->getRichTbPhotonInfo();
                if(aRichTbPhotonInfo) {
                 G4double aChTrackTotMom = aRichTbPhotonInfo->ParentChTrackMomentum();
                 G4ThreeVector aPhotTIR =aRichTbPhotonInfo-> PhotonCrystalDnsTIRCoord();
                 G4ThreeVector aPhotMIR =aRichTbPhotonInfo-> PhotonSphMirrReflCoord();
                 G4ThreeVector aPhotRFR =aRichTbPhotonInfo-> PhotonCrystalDnsExitCoord();

                 // G4cout<<" aPhotTIR "<<aPhotTIR <<G4endl;

                 if( aChTrackTotMom > ( mChTrackMinMomFactorForHisto* (aRunConfig-> getRichTbParticleMomentum()))) {

                   if(myanalysisManager ->getfEmisPtPrimaryTrackPhoton())
                      myanalysisManager ->getfEmisPtPrimaryTrackPhoton()->Fill(emisptZ);
                   G4double aCkvAtProd = acos(aRichTbPhotonInfo->CkvCosThetaAtProd());

                   if(myanalysisManager ->getfGeneratedCkvPhoton() )
                     myanalysisManager ->getfGeneratedCkvPhoton() ->Fill(aCkvAtProd);
                   // G4cout<<" aPhotTIR Fill "<<aPhotTIR <<G4endl;

                   if(myanalysisManager ->getfTIRXYLocationPhoton() )
                     myanalysisManager ->getfTIRXYLocationPhoton()->Fill (aPhotTIR.x(), aPhotTIR.y() );

                   if(myanalysisManager ->getfMIRXYLocationPhoton() )
                     myanalysisManager ->getfMIRXYLocationPhoton()->Fill (aPhotMIR.x(), aPhotMIR.y() );

                   if(myanalysisManager ->getfRFRXYLocationPhoton() )
                     myanalysisManager ->getfRFRXYLocationPhoton()->Fill (aPhotRFR.x(), aPhotRFR.y() );



                 }

                }


               }


            }


		      }

		    }


        }
      }
       }
     }







        /*
		    //angle on frame
		    const G4ThreeVector CurPhotDir = aTrack ->GetMomentumDirection() ;
		    G4cout<<" Photon direction "<< CurPhotDir <<G4endl;
		    G4double IncidenceAngle = asin(-CurPhotDir.x()/sqrt( CurPhotDir.x()*CurPhotDir.x() +
						       CurPhotDir.z()*CurPhotDir.z()));
		    if( myanalysisManager -> getPhotonZAngle()) {
		      myanalysisManager -> getPhotonZAngle()->Fill( IncidenceAngle);
		    }
		    G4double TiltAngle = - aRunConfig->getSpecialStudyCrystalRotationY();
		    G4double CherenkovAngle = asin( sin( IncidenceAngle - TiltAngle )/1.5) + TiltAngle;
		    G4cout<<"Cherenkov angle  "<<CherenkovAngle <<G4endl;
		    if( myanalysisManager -> getCherenkovAngle()) {
		      myanalysisManager -> getCherenkovAngle()->Fill(CherenkovAngle);
		    }

		    //radius as a function of lambda
		    const G4ThreeVector CurPhotMom = aTrack ->GetMomentum() ;

		    G4double Energy = sqrt( CurPhotMom.x()*CurPhotMom.x() + CurPhotMom.y()*CurPhotMom.y() +
						       CurPhotMom.z()*CurPhotMom.z());
		    G4double Lambda = 1243.125*(CLHEP::nanometer*CLHEP::eV)/Energy*1000000;
		    G4cout<<"Energy  "<< Energy <<G4endl;
		    G4cout<<"Lambda  "<< Lambda <<G4endl;
		    if( myanalysisManager -> getSpectrum()) {
		      myanalysisManager -> getSpectrum()->Fill(Lambda);
		    }
		    if( myanalysisManager -> getRadiusVsWL()) {
		      if(postPos.y()>-30.0 && postPos.y()< +30.0){
			if(postPos.x()>-60.0 && postPos.x()< -20.0){
			double y = postPos.y() - 0.0;
			double x = postPos.x() + 0.0;
			double radius = sqrt(y*y + x*x);
			myanalysisManager -> getRadiusVsWL()->Fill(Lambda,radius);
			if( myanalysisManager -> getRadiusVsWLProfile()) {
			  myanalysisManager -> getRadiusVsWLProfile()->Fill(Lambda,radius);
			}
			}
		      }
		    }



		    //I just keep central hits
		    if(postPos.y()>-50.0 && postPos.y()< +50.0){

		      if( myanalysisManager -> getPhotonXYCoordProfileCut()) {
			 myanalysisManager -> getPhotonXYCoordProfileCut()->Fill(postPos.y(), postPos.x());
		       }
		      if( myanalysisManager -> getCherenkovAngleCut()) {
			myanalysisManager -> getCherenkovAngleCut()->Fill(CherenkovAngle);
		      }
		    }
		  }
		    // check post mirror
		    if(tpreVol == MirrorPhysName &&
		       tpostVol == CrystalMasterPhysName ) {



		      //radius as a function of lambda
		      const G4ThreeVector CurPhotMom = aTrack ->GetMomentum() ;

		      G4double Energy = sqrt( CurPhotMom.x()*CurPhotMom.x() + CurPhotMom.y()*CurPhotMom.y() +
						       CurPhotMom.z()*CurPhotMom.z());
		      G4double Lambda = 1243.125*(CLHEP::nanometer*CLHEP::eV)/Energy*1000000;
		      G4cout<<"Energy post mirror "<< Energy <<G4endl;
		      G4cout<<"Lambda post mirror "<< Lambda <<G4endl;
		      if( myanalysisManager -> getSpectrumPostMirror()) {
			myanalysisManager -> getSpectrumPostMirror()->Fill(Lambda);
		      }
		    }

		   // check pre mirror
		    if(tpreVol == CrystalMasterPhysName &&
		       tpostVol == MirrorPhysName ) {



		      //radius as a function of lambda
		      const G4ThreeVector CurPhotMom = aTrack ->GetMomentum() ;

		      G4double Energy = sqrt( CurPhotMom.x()*CurPhotMom.x() + CurPhotMom.y()*CurPhotMom.y() +
						       CurPhotMom.z()*CurPhotMom.z());
		      G4double Lambda = 1243.125*(CLHEP::nanometer*CLHEP::eV)/Energy*1000000;
		      G4cout<<"Energy pre mirror "<< Energy <<G4endl;
		      G4cout<<"Lambda pre mirror "<< Lambda <<G4endl;
		      if( myanalysisManager -> getSpectrumPreMirror()) {
			myanalysisManager -> getSpectrumPreMirror()->Fill(Lambda);
		      }
		    }


		  }

	      }
        */




}


