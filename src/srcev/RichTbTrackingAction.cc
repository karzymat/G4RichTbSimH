#include "RichTbTrackingAction.hh"
#include "Geant4/G4DynamicParticle.hh"
#include "Geant4/G4ParticleDefinition.hh"
#include "Geant4/G4ThreeVector.hh"
#include "Geant4/G4OpticalPhoton.hh"
#include "Geant4/globals.hh"
#include "RichTbUserTrackInfo.hh"
#include "RichTbPhotoElectron.hh"
#include "RichTbBeamProperty.hh"
#include "RichTbAnalysisManager.hh"
#include "RichTbRunConfig.hh"

RichTbTrackingAction::RichTbTrackingAction()
{;}
RichTbTrackingAction::~RichTbTrackingAction()
{  ;}
void RichTbTrackingAction::PreUserTrackingAction(const G4Track * /* aTrack */) 
{;}
void RichTbTrackingAction:: PostUserTrackingAction(const G4Track * aTrack)
{
  if( aTrack != 0 ) {
  
   const G4DynamicParticle * aParticle = aTrack->GetDynamicParticle();
   if( aParticle->GetDefinition() == G4OpticalPhoton::OpticalPhoton() ){

   // delete the photon user info at the end of
     // tracking the photon. This is so that it does not
     // accumulate over the events and fill up the memory.
     // This removes dangling reference to the trackuserinformation class.
     // no deletion for now. SE 20-1-2004.

     //  G4double cPhotTrueCkv = 0.0;
     
     G4VUserTrackInformation* aTkInfo = aTrack->GetUserInformation();
     
     if( aTkInfo) {
       RichTbUserTrackInfo*  curPhotTrackUserInfo =
                 (RichTbUserTrackInfo*)  aTkInfo;

       if(curPhotTrackUserInfo && curPhotTrackUserInfo->HasUserPhotonInfo() ){
           RichTbPhotonInfo* aRichTbPhotonInfo = 
              curPhotTrackUserInfo->getRichTbPhotonInfo();
           if( aRichTbPhotonInfo ) {

             // cPhotTrueCkv=aRichTbPhotonInfo->CkvCosThetaAtProd();
             
             //  delete   aRichTbPhotonInfo;
             
           }
           

       }
       
     }
     
     
   } else if (aParticle->GetDefinition() ==  
              RichTbPhotoElectron::PhotoElectron()) {
     // delete the photoelectron user info at the end of tracking the
     // photoelectron. This is so that they  do not accumulate 
     // over the events and fill up the memory.
     // This removes dangling reference to the trackuserinformation class.
     // no deletion for now. SE 20-1-2004.
     G4VUserTrackInformation* aTkInfoP = aTrack->GetUserInformation();
     if( aTkInfoP) {

       RichTbUserTrackInfo*  curPeTrackUserInfo =
                 (RichTbUserTrackInfo*)  aTkInfoP;
    
       if( curPeTrackUserInfo && curPeTrackUserInfo-> HasUserPEInfo()) {
           RichTbPEInfo* aRichTbPEInfo =
             curPeTrackUserInfo->getRichTbPEInfo();
           if(aRichTbPEInfo ) {
             //  delete   aRichTbPEInfo;
             
           }
           

       }
       
       
     }
     
   }   
      
    
  }
  
  
}
