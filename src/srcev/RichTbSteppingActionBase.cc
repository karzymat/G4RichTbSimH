// Include files 



// local


//-----------------------------------------------------------------------------
// Implementation file for class : RichTbSteppingActionBase
//
// 2015-03-06 : Sajan Easo
//-----------------------------------------------------------------------------
#include "Geant4/globals.hh"
#include "RichTbSteppingActionBase.hh"
#include "Geant4/G4SteppingManager.hh"

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RichTbSteppingActionBase::RichTbSteppingActionBase(  ) {

  InitRichTbStepActions();
  
 

}
//=============================================================================
// Destructor
//=============================================================================
RichTbSteppingActionBase::~RichTbSteppingActionBase() {

  if(mRichTbSteppingAction) delete mRichTbSteppingAction;
  if(mRichTbPmtSteppingAction) delete mRichTbPmtSteppingAction;

} 

//=============================================================================
void RichTbSteppingActionBase::UserSteppingAction(const G4Step * aStep){

  mRichTbSteppingAction->UserSteppingAction(aStep);
  mRichTbPmtSteppingAction->UserSteppingAction(aStep);



}

void  RichTbSteppingActionBase::InitRichTbStepActions(){

  mRichTbSteppingAction=new RichTbSteppingAction();
  mRichTbPmtSteppingAction = new RichTbPmtSteppingAction();
  
  

}
