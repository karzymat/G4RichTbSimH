// Include files

#include "RichTbGeometryParameters.hh"
#include "RichTbMiscNames.hh"
#include "RichTbMaterial.hh"
#include "Geant4/G4RotationMatrix.hh"
#include "Geant4/G4ThreeVector.hh"
#include "Geant4/G4Transform3D.hh"
#include "Geant4/G4SubtractionSolid.hh"
#include "Geant4/G4Box.hh"
#include "Geant4/G4PVPlacement.hh"
#include "RichTbRunConfig.hh"



// local
#include "RichTbUpgradeEC.hh"

//-----------------------------------------------------------------------------
// Implementation file for class : RichTbUpgradeEC
//
// 2014-10-23 : Sajan Easo
// 2015-06-11 : Michele Blago
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
RichTbUpgradeEC::RichTbUpgradeEC(RichTbUpgradePhDetSupFrame * rTbPhotSupFrame   ) {

   aRTbPhotSupFrame = rTbPhotSupFrame ;
   // constructRichTbUpgradeEC ();
   // constructRichTbUpgradeECSupport ();


}
//=============================================================================
// Destructor
//=============================================================================
RichTbUpgradeEC::~RichTbUpgradeEC() {}

//=============================================================================
void RichTbUpgradeEC::constructRichTbUpgradeEC () {
   RichTbMaterial* aMaterial = RichTbMaterial::getRichTbMaterialInstance();
   G4Box * ECBox = new G4Box("ECBox", 0.5*ECXSize,0.5*ECYSize, 0.5*ECZSize);
   G4RotationMatrix ECBoxRotX, ECBoxRotY;
   G4ThreeVector ECPosLeft(ECXLocation ,ECYLocation ,ECZLocation);
   G4ThreeVector ECPosRight(ECXLocation ,ECYLocation ,ECZLocation);
   G4Transform3D ECTransformLeft( ECBoxRotX*ECBoxRotY, ECPosLeft);
   G4Transform3D ECTransformRight( ECBoxRotX*ECBoxRotY, ECPosRight);

   G4LogicalVolume*  ECLogLeft = new G4LogicalVolume( ECBox, aMaterial->getNitrogenGas(),
                                                      "ECLeftLog", 0,0,0);
   G4LogicalVolume*  ECLogRight = new G4LogicalVolume( ECBox, aMaterial->getNitrogenGas(),
                                                      "ECRightLog", 0,0,0);
   G4VPhysicalVolume* ECPhysLeft = new G4PVPlacement(ECTransformLeft, "ECLeftPhys",
                                                     ECLogLeft, aRTbPhotSupFrame->getRichTbPhDetSupFrameLeftPVol(),false,0);

   G4VPhysicalVolume* ECPhysRight = new G4PVPlacement(ECTransformRight, "ECRightPhys",
                                                     ECLogRight, aRTbPhotSupFrame->getRichTbPhDetSupFrameRightPVol(),false,1);


   RichTbECLeftLVol  =   ECLogLeft;
   RichTbECRightLVol =   ECLogRight;
   RichTbECLeftPVol  =   ECPhysLeft;
   RichTbECRightPVol =  ECPhysRight;

}
void RichTbUpgradeEC::constructRichTbUpgradeECSupport () {

   RichTbMaterial* aMaterial = RichTbMaterial::getRichTbMaterialInstance();
   G4Box * ECSupBox = new G4Box("ECBox", 0.5*ECSupportXSize,0.5*ECSupportYSize, 0.5*ECSupportZSize);

   G4ThreeVector ECSupPos(ECXLocation ,ECYLocation , ECSupportZLocation);

   G4RotationMatrix ECSupportBoxRot;
   G4Transform3D ECSupTransform( ECSupportBoxRot, ECSupPos);
   G4LogicalVolume*  ECSupLog  = new G4LogicalVolume( ECSupBox, aMaterial->getCarbon(),
                                                      "ECSupLog", 0,0,0);

   G4VPhysicalVolume* ECSupPhysLeft = new G4PVPlacement(ECSupTransform, "ECSupLeftPhys",
                                                     ECSupLog, aRTbPhotSupFrame->getRichTbPhDetSupFrameLeftPVol(),false,0);

   G4VPhysicalVolume* ECSupPhysRight = new G4PVPlacement(ECSupTransform, "ECSupRightPhys",
                                                     ECSupLog, aRTbPhotSupFrame->getRichTbPhDetSupFrameRightPVol(),false,0);

   RichTbECSupLVol      =  ECSupLog ;
   RichTbECSupLeftPVol  =  ECSupPhysLeft;
   RichTbECSupRightPVol =  ECSupPhysRight;


}
void RichTbUpgradeEC::constructRichTbUpgradeSingleEC () {
   RichTbMaterial* aMaterial = RichTbMaterial::getRichTbMaterialInstance();
   G4Box * ECBox = new G4Box("ECBox", 0.5*ECXSize,0.5*ECYSize, 0.5*ECZSize);
   G4RotationMatrix ECBoxRotX, ECBoxRotY;
   G4ThreeVector ECPosLeft(ECXLocation ,ECYLocation ,ECZLocation);
   G4Transform3D ECTransformLeft( ECBoxRotX*ECBoxRotY, ECPosLeft);

   G4LogicalVolume*  ECLogLeft = new G4LogicalVolume( ECBox, aMaterial->getNitrogenGas(),
                                                      "ECLeftLog", 0,0,0);
   G4VPhysicalVolume* ECPhysLeft = new G4PVPlacement(ECTransformLeft, "ECLeftPhys",
                                                     ECLogLeft, aRTbPhotSupFrame->getRichTbPhDetSupFrameLeftPVol(),false,0);



   RichTbECLeftLVol  =   ECLogLeft;
   RichTbECLeftPVol  =   ECPhysLeft;

}
void RichTbUpgradeEC::constructRichTbUpgradeSingleECSupport () {

   RichTbMaterial* aMaterial = RichTbMaterial::getRichTbMaterialInstance();
   G4Box * ECSupBox = new G4Box("ECBox", 0.5*ECSupportXSize,0.5*ECSupportYSize, 0.5*ECSupportZSize);

   G4ThreeVector ECSupPos(ECXLocation ,ECYLocation , ECSupportZLocation);

   G4RotationMatrix ECSupportBoxRot;
   G4Transform3D ECSupTransform( ECSupportBoxRot, ECSupPos);
   G4LogicalVolume*  ECSupLog  = new G4LogicalVolume( ECSupBox, aMaterial->getCarbon(),
                                                      "ECSupLog", 0,0,0);

   G4VPhysicalVolume* ECSupPhysLeft = new G4PVPlacement(ECSupTransform, "ECSupLeftPhys",
                                                     ECSupLog, aRTbPhotSupFrame->getRichTbPhDetSupFrameLeftPVol(),false,0);


   RichTbECSupLVol      =  ECSupLog ;
   RichTbECSupLeftPVol  =  ECSupPhysLeft;



}
void RichTbUpgradeEC::constructRichTbUpgradeEC15() {
   RichTbMaterial* aMaterial = RichTbMaterial::getRichTbMaterialInstance();
   G4Box * ECBox = new G4Box("ECBox", 0.5*ECXSize,0.5*ECYSize, 0.5*ECZSize);
   G4RotationMatrix ECBoxRotX, ECBoxRotY;
   G4ThreeVector ECPosLeft(ECXLocation15,ECYLocation15,ECZLocation15);
   G4ThreeVector ECPosRight(ECXLocation15,ECYLocation15,ECZLocation15);
   G4ThreeVector ECPosBottomLeft(ECXLocation15,ECYLocation15,ECZLocation15);
   G4ThreeVector ECPosBottomRight(ECXLocation15,ECYLocation15,ECZLocation15);
   G4Transform3D ECTransformLeft(ECBoxRotX*ECBoxRotY,ECPosLeft);
   G4Transform3D ECTransformRight(ECBoxRotX*ECBoxRotY,ECPosRight);
   G4Transform3D ECTransformBottomLeft(ECBoxRotX*ECBoxRotY,ECPosBottomLeft);
   G4Transform3D ECTransformBottomRight(ECBoxRotX*ECBoxRotY,ECPosBottomRight);

   G4LogicalVolume*  ECLogLeft = new G4LogicalVolume( ECBox, aMaterial->getNitrogenGas(),
                                                      "ECLeftLog", 0,0,0);
   G4LogicalVolume*  ECLogRight = new G4LogicalVolume( ECBox, aMaterial->getNitrogenGas(),
                                                      "ECRightLog", 0,0,0);
   G4LogicalVolume*  ECLogBottomLeft = new G4LogicalVolume(ECBox, aMaterial->getNitrogenGas(),
                                                      "ECBottomLeftLog", 0,0,0);
   G4LogicalVolume*  ECLogBottomRight = new G4LogicalVolume(ECBox, aMaterial->getNitrogenGas(),
                                                      "ECBottomRightLog", 0,0,0);

   G4VPhysicalVolume* ECPhysLeft = new G4PVPlacement(ECTransformLeft, "ECLeftPhys",
                                                     ECLogLeft, aRTbPhotSupFrame->getRichTbPhDetSupFrameLeftPVol(),false,0);
   G4VPhysicalVolume* ECPhysRight = new G4PVPlacement(ECTransformRight, "ECRightPhys",
                                                     ECLogRight, aRTbPhotSupFrame->getRichTbPhDetSupFrameRightPVol(),false,1);
   G4VPhysicalVolume* ECPhysBottomLeft = new G4PVPlacement(ECTransformBottomLeft, "ECBottomLeftPhys",
                                                     ECLogBottomLeft, aRTbPhotSupFrame->getRichTbPhDetSupFrameBottomLeftPVol(),false,2);
   G4VPhysicalVolume* ECPhysBottomRight = new G4PVPlacement(ECTransformBottomRight, "ECRBottomightPhys",
                                                     ECLogBottomRight, aRTbPhotSupFrame->getRichTbPhDetSupFrameBottomRightPVol(),false,3);


   RichTbECLeftLVol  = ECLogLeft;
   RichTbECRightLVol = ECLogRight;
   RichTbECBottomLeftLVol  = ECLogBottomLeft;
   RichTbECBottomRightLVol = ECLogBottomRight;

   RichTbECLeftPVol  = ECPhysLeft;
   RichTbECRightPVol = ECPhysRight;
   RichTbECBottomLeftPVol  = ECPhysBottomLeft;
   RichTbECBottomRightPVol = ECPhysBottomRight;

}
void RichTbUpgradeEC::constructRichTbUpgradeECSupport15() {

   RichTbMaterial* aMaterial = RichTbMaterial::getRichTbMaterialInstance();
   G4Box * ECSupBox = new G4Box("ECBox", 0.5*ECSupportXSize,0.5*ECSupportYSize, 0.5*ECSupportZSize);

   G4ThreeVector ECSupPos(ECSupportXLocation15 ,ECSupportYLocation15 , ECSupportZLocation15);
   G4ThreeVector ECSupPosBottom(ECSupportXLocation15 ,ECSupportYLocation15 , ECSupportZLocation15);

   G4RotationMatrix ECSupportBoxRot;
   G4Transform3D ECSupTransform( ECSupportBoxRot, ECSupPos);
   G4Transform3D ECSupTransformBottom( ECSupportBoxRot, ECSupPos);
   G4LogicalVolume*  ECSupLog  = new G4LogicalVolume( ECSupBox, aMaterial->getCarbon(),
                                                      "ECSupLog", 0,0,0);

   G4VPhysicalVolume* ECSupPhysLeft = new G4PVPlacement(ECSupTransform, "ECSupLeftPhys",
                                                     ECSupLog, aRTbPhotSupFrame->getRichTbPhDetSupFrameLeftPVol(),false,0);
   G4VPhysicalVolume* ECSupPhysRight = new G4PVPlacement(ECSupTransform, "ECSupRightPhys",
                                                     ECSupLog, aRTbPhotSupFrame->getRichTbPhDetSupFrameRightPVol(),false,1);

   G4VPhysicalVolume* ECSupPhysBottomLeft = new G4PVPlacement(ECSupTransformBottom, "ECSupBottomLeftPhys",
                                                     ECSupLog, aRTbPhotSupFrame->getRichTbPhDetSupFrameBottomLeftPVol(),false,2);
   G4VPhysicalVolume* ECSupPhysBottomRight = new G4PVPlacement(ECSupTransformBottom, "ECSupBottomRightPhys",
                                                     ECSupLog, aRTbPhotSupFrame->getRichTbPhDetSupFrameBottomRightPVol(),false,3);

   RichTbECSupLVol      =  ECSupLog ;
   RichTbECSupLeftPVol  =  ECSupPhysLeft;
   RichTbECSupRightPVol =  ECSupPhysRight;
   RichTbECSupBottomLeftPVol  =  ECSupPhysBottomLeft;
   RichTbECSupBottomRightPVol =  ECSupPhysBottomRight;
}
