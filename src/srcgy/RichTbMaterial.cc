
// CLHEP
#include "CLHEP/Units/PhysicalConstants.h"

// Geant 4
#include "Geant4/globals.hh"
#include "Geant4/G4Isotope.hh"
#include "Geant4/G4Element.hh"
#include "Geant4/G4ElementTable.hh"
#include "Geant4/G4Material.hh"
#include "Geant4/G4MaterialTable.hh"
#include "Geant4/G4UnitsTable.hh"
#include "Geant4/G4OpticalSurface.hh"
#include "Geant4/G4LogicalBorderSurface.hh"
#include "Geant4/G4LogicalSkinSurface.hh"
#include "Geant4/G4OpBoundaryProcess.hh"

// STL etc
#include <iostream>
#include <vector>
// local
#include "RichTbMaterial.hh"
#include "RichTbMaterialParameters.hh"
#include "RichTbRunConfig.hh"
//#include "RichTbGasQuWinProperty.hh"
#include "RichTbAnalysisManager.hh"


RichTbMaterial* RichTbMaterial::RichTbMaterialInstance =0;

RichTbMaterial::RichTbMaterial( ){

  InitializeRichTbMaterial();
  
  RichTbRunConfig* aRunConfig= RichTbRunConfig::getRunConfigInstance();
  
  //  RichTbGasQuWinProperty* aRichTbGasQuWinProperty= RichTbGasQuWinProperty::getRichTbGasQuWinPropertyInstance();

  HistoRichMaterialProperty();

    G4double a, z, density;     //a=mass of a CLHEP::mole;
    // z=mean number of protons;
    G4String name, symbol;
    //  G4int isz, isn;             //isz= number of protons in an isotope;
    //isn= number of nucleons in an isotope;

    G4int numel, natoms;        //numel=Number of elements constituting a material.
    //   G4double abundance;
    G4double fractionmass;
    G4double temperature, pressure;
    G4State aStateGas =  kStateGas;
    // G4double FactorOne = 1.0;
    G4UnitDefinition::BuildUnitsTable();

    //PhotonEnergy

        G4double PhotonEnergyStep = 
       (PhotonMaxEnergy - PhotonMinEnergy) / NumPhotWaveLengthBins;
       G4double *PhotonMomentum = new G4double[NumPhotWaveLengthBins];
       for (G4int ibin = 0; ibin < NumPhotWaveLengthBins; ibin++) {
        PhotonMomentum[ibin] = PhotonMinEnergy + PhotonEnergyStep * ibin;
     }
     G4int IbMinForCkv= PhotEnerBinEdgeForSafety;
     G4int IbMaxForCkv= NumPhotWaveLengthBins-PhotEnerBinEdgeForSafety;
     //     G4double PhotEneryStepForCkvProd=
     //  ( PhotonCkvProdMaxEnergy- PhotonCkvProdMinEnergy)/
     //  CkvProdNumPhotWaveLengthBins;     

       G4double* PhotonMomentumCkvProd = 
           new G4double[CkvProdNumPhotWaveLengthBins];
       for(G4int ibinc=IbMinForCkv;  ibinc<IbMaxForCkv; ibinc++){
         G4int ibp= ibinc- PhotEnerBinEdgeForSafety;
         PhotonMomentumCkvProd[ibp]=PhotonMomentum[ibinc];         
       }
       
       

    G4cout << "\nNow Define Elements ..\n" << G4endl;

    // Nitrogen

    a = 14.01 * CLHEP::g / CLHEP::mole;
    G4Element *elN = new G4Element(name = "Nitrogen",
                                   symbol = "N", z = 7., a);

    //Oxygen

    a = 16.00 * CLHEP::g / CLHEP::mole;
    G4Element *elO = new G4Element(name = "Oxygen",
                                   symbol = "O", z = 8., a);

    //Hydrogen

    a = 1.01 * CLHEP::g / CLHEP::mole;
    G4Element *elH = new G4Element(name = "Hydrogen",
                                   symbol = "H", z = 1., a);

    //Carbon

    a = 12.01 * CLHEP::g / CLHEP::mole;
    G4Element *elC = new G4Element(name = "Carbon",
                                   symbol = "C", z = 6., a);

    //Silicon

    a = 28.09 * CLHEP::g / CLHEP::mole;
    G4Element *elSi = new G4Element(name = "Silicon",
                                    symbol = "Si", z = 14., a);
    //Fluorine
    a = 18.998 * CLHEP::g / CLHEP::mole;
    G4Element *elF = new G4Element(name = "Fluorine",
                                   symbol = "F", z = 9., a);
    //Aluminum
    a = 26.98 * CLHEP::g / CLHEP::mole;
    G4Element *elAL = new G4Element(name = "Aluminium",
                                    symbol = "Al", z = 13., a);

    //Sodium
    a = 22.99 * CLHEP::g / CLHEP::mole;
    G4Element *elNa = new G4Element(name = "Sodium",
                                    symbol = "Na", z = 11., a);

    //Potassium
    a = 39.10 * CLHEP::g / CLHEP::mole;
    G4Element *elK = new G4Element(name = "Potassium",
                                   symbol = "K", z = 19., a);

    //Cesium

    a = 132.91 * CLHEP::g / CLHEP::mole;
    G4Element *elCs = new G4Element(name = "Cesium",
                                    symbol = "Cs", z = 55., a);

    //Antimony

    a = 121.76 * CLHEP::g / CLHEP::mole;
    G4Element *elSb = new G4Element(name = "Antimony",
                                    symbol = "Sb", z = 51., a);

    //Define Materials
    G4cout << "\nNow Define Materials ..\n" << G4endl;
    //

    //Air at 20 degree C and 1 atm for the ambiet air.
    // Also Air as  a radiator material for inside the tubes.
    //--
    density = 1.205e-03 * CLHEP::g / CLHEP::cm3;
    pressure = 1. * CLHEP::atmosphere;
    temperature = 293. * CLHEP::kelvin;
    G4Material *Air = new G4Material(name = "Air ", density, numel = 2,
                                     aStateGas, temperature, pressure);
    Air->AddElement(elN, fractionmass = 0.7);
    Air->AddElement(elO, fractionmass = 0.3);


    RichTbAmbientAir = Air;

    density = 1.205e-03 * CLHEP::g / CLHEP::cm3;
    pressure = 1. * CLHEP::atmosphere;
    temperature = 293. * CLHEP::kelvin;
    G4Material *TAir = new G4Material(name = "TAir ", density, numel = 2,
                                      aStateGas, temperature, pressure);
    TAir->AddElement(elN, fractionmass = 0.7);
    TAir->AddElement(elO, fractionmass = 0.3);

    G4double *TAirAbsorpLength = new G4double[NumPhotWaveLengthBins];
    G4double *TAirRindex = new G4double[NumPhotWaveLengthBins];

    for (G4int ibin = 0; ibin < NumPhotWaveLengthBins; ibin++) {
        TAirAbsorpLength[ibin] = 1.E32 * CLHEP::mm;
        TAirRindex[ibin] = 1.000273;
    }
    G4MaterialPropertiesTable *TAirMPT = new G4MaterialPropertiesTable();

    TAirMPT->AddProperty("ABSLENGTH", PhotonMomentum, 
                  TAirAbsorpLength, NumPhotWaveLengthBins);

    TAirMPT->AddProperty("RINDEX", PhotonMomentum, TAirRindex, 
                        NumPhotWaveLengthBins);

    TAir->SetMaterialPropertiesTable(TAirMPT);
    RichTbTubeAir = TAir;

    //
    //

    density = 1.205e-03 * CLHEP::g / CLHEP::cm3;
    pressure = 1. * CLHEP::atmosphere;
    temperature = 293. * CLHEP::kelvin;
    G4Material *TAirA = new G4Material(name = "TAirA ", density, numel = 2,
                                       aStateGas, temperature, pressure);
    TAirA->AddElement(elN, fractionmass = 0.7);
    TAirA->AddElement(elO, fractionmass = 0.3);

    G4double *TAirAAbsorpLength = new G4double[NumPhotWaveLengthBins];
    // G4double *TAirARindex = new G4double[NumPhotWaveLengthBins];

    for (G4int ibin = 0; ibin < NumPhotWaveLengthBins; ibin++) {
        TAirAAbsorpLength[ibin] = 1.E32 * CLHEP::mm;
        //    TAirARindex[ibin]=1.0;
        //    TAirARindex[ibin]=1.000273;
    }
    G4MaterialPropertiesTable *TAirAMPT = new G4MaterialPropertiesTable();

    TAirAMPT->AddProperty("ABSLENGTH", PhotonMomentum, 
                              TAirAAbsorpLength, NumPhotWaveLengthBins);

    //    TAirAMPT->AddProperty("RINDEX", PhotonMomentum,
    //               TAirARindex,NumPhotWaveLengthBins);

    TAirA->SetMaterialPropertiesTable(TAirAMPT);
    RichTbAirA = TAirA;

    //    HistoRichTbMaterialProperties(rAnalysis, rConfig);

    //Water
    
 density=1.000*CLHEP::g/CLHEP::cm3;
 G4Material* H2O = new G4Material(name="Water",density,numel=2);
 H2O->AddElement(elH,natoms=2);
 H2O->AddElement(elO,natoms=1);
    G4double *H20AbsorpLength = new G4double[NumPhotWaveLengthBins];
    G4double *H20Rindex = new G4double[NumPhotWaveLengthBins];

    G4MaterialPropertiesTable *H20AMPT = new G4MaterialPropertiesTable();
    for (G4int ibin = 0; ibin < NumPhotWaveLengthBins; ibin++) {
      H20AbsorpLength[ibin]=100000.0*CLHEP::mm  ;
         H20Rindex[ibin]=1.33;
       
    }
    H20AMPT->AddProperty("ABSLENGTH",PhotonMomentum,
                         H20AbsorpLength, NumPhotWaveLengthBins);

    H20AMPT->AddProperty("RINDEX", PhotonMomentum,
                         H20Rindex,NumPhotWaveLengthBins);

    H2O->SetMaterialPropertiesTable(H20AMPT);
    



  //Nitrogen gas.

  density = 0.8073e-03*CLHEP::g/CLHEP::cm3;
  //  pressure=1.*CLHEP::atmosphere;
  // temperature=293.*CLHEP::kelvin;
  pressure = aRunConfig -> getNitrogenPressure();
  
  temperature = aRunConfig ->getNitrogenTemperature();
  

  G4Material* NitrogenGas = new G4Material(name=NitrogenGasMaterialName, 
                                 density, numel=1,
                                 kStateGas,temperature,pressure);
  NitrogenGas->AddElement(elN, natoms=2);

  G4double* NitrogenGasAbsorpLength=new G4double[NumPhotWaveLengthBins];
  G4double* NitrogenGasRindex=new G4double[NumPhotWaveLengthBins];
  G4double* NitrogenGasPhotMom=new G4double[NumPhotWaveLengthBins];

  G4double* NitrogenGasCkvProdRindex=
          new G4double[CkvProdNumPhotWaveLengthBins];
  G4double* NitrogenGasPhotMomCkvProd=
          new G4double[CkvProdNumPhotWaveLengthBins];
  

  std::vector<G4double>N2RefInd= InitN2RefIndex(pressure,temperature);
  std::vector<G4double>N2RefPhotMom=InitN2RefPhotMom();


  for (G4int ibin=0; ibin<NumPhotWaveLengthBins; ibin++){
    NitrogenGasAbsorpLength[ibin]=1.E32*CLHEP::mm;

    NitrogenGasRindex[ibin]=N2RefInd[ibin];
    NitrogenGasPhotMom[ibin]=N2RefPhotMom[ibin];
    // G4cout<<" N2 ref index in RichMaterial "<<ibin<<"  "<< NitrogenGasPhotMom[ibin]	  <<"  "<<NitrogenGasRindex[ibin]<<G4endl;

  }
  
  for (G4int ibina=IbMinForCkv; ibina <IbMaxForCkv ; ibina++){
    G4int ibinp=  ibina-PhotEnerBinEdgeForSafety;
    
    NitrogenGasCkvProdRindex[ibinp]= NitrogenGasRindex[ibina];
    NitrogenGasPhotMomCkvProd[ibinp]=  NitrogenGasPhotMom[ibina];
    
  }
  
  G4MaterialPropertiesTable* NitrogenGasMPT = 
                            new G4MaterialPropertiesTable();

    NitrogenGasMPT->AddProperty("ABSLENGTH",NitrogenGasPhotMom,
                        NitrogenGasAbsorpLength,NumPhotWaveLengthBins);

    NitrogenGasMPT->AddProperty("RINDEX", NitrogenGasPhotMom, 
                      NitrogenGasRindex,NumPhotWaveLengthBins);

    NitrogenGasMPT->AddProperty("CKVRNDX", NitrogenGasPhotMomCkvProd , 
                      NitrogenGasCkvProdRindex,CkvProdNumPhotWaveLengthBins);

    NitrogenGas->SetMaterialPropertiesTable(NitrogenGasMPT);
    RichTbNitrogenGas = NitrogenGas;

    // test printout

    // G4MaterialPropertyVector* theRefractionIndexVector = 
    //		    	  NitrogenGasMPT ->GetProperty("CKVRNDX");

    //  G4int itb=0;
    //  theRefractionIndexVector->ResetIterator();
    //  while(++(*theRefractionIndexVector))

    //  {
    // itb++;
                               
    //  G4double currentPM = theRefractionIndexVector->GetPhotonMomentum();
    //  G4double currentRI=theRefractionIndexVector->GetProperty();


    // G4cout<<" Richtbmaterial N2refindVect  "<< itb<<"  "<< currentPM<<"  "<<currentRI
    //      <<G4endl;

    //  }  

  // end testprintout
  //c4f10 gas.

  density = 0.01195*CLHEP::g/CLHEP::cm3;
  //  pressure=1.*CLHEP::atmosphere;
  // temperature=293.*CLHEP::kelvin;
  pressure = aRunConfig -> getc4f10Pressure();
  
  temperature = aRunConfig ->getc4f10Temperature();
  

  G4Material* c4f10Gas = new G4Material(name=c4f10GasMaterialName, 
                                 density, numel=2,
                                 kStateGas,temperature,pressure);
  c4f10Gas->AddElement(elC, natoms=4);
  c4f10Gas->AddElement(elF, natoms=10);

  G4double* c4f10GasAbsorpLength=new G4double[NumPhotWaveLengthBins];
  G4double* c4f10GasRindex=new G4double[NumPhotWaveLengthBins];
  G4double* c4f10GasPhotMom=new G4double[NumPhotWaveLengthBins];

  G4double* c4f10GasCkvProdRindex=
          new G4double[CkvProdNumPhotWaveLengthBins];
  G4double* c4f10GasPhotMomCkvProd=
          new G4double[CkvProdNumPhotWaveLengthBins];
  

  std::vector<G4double>c4f10RefInd= Initc4f10RefIndex(pressure,temperature);
  std::vector<G4double>c4f10RefPhotMom=Initc4f10RefPhotMom();


  for (G4int ibin=0; ibin<NumPhotWaveLengthBins; ibin++){
    c4f10GasAbsorpLength[ibin]=1.E32*CLHEP::mm;

    c4f10GasRindex[ibin]=c4f10RefInd[ibin];
    c4f10GasPhotMom[ibin]=c4f10RefPhotMom[ibin];

  }
  
  for (G4int ibina=IbMinForCkv; ibina <IbMaxForCkv ; ibina++){
    G4int ibinp=  ibina-PhotEnerBinEdgeForSafety;
    
    c4f10GasCkvProdRindex[ibinp]= c4f10GasRindex[ibina];
    c4f10GasPhotMomCkvProd[ibinp]=  c4f10GasPhotMom[ibina];
    
  }
  
  G4MaterialPropertiesTable* c4f10GasMPT = 
                            new G4MaterialPropertiesTable();

     c4f10GasMPT->AddProperty("ABSLENGTH",c4f10GasPhotMom,
                        c4f10GasAbsorpLength,NumPhotWaveLengthBins);

     c4f10GasMPT->AddProperty("RINDEX", c4f10GasPhotMom, 
                      c4f10GasRindex,NumPhotWaveLengthBins);

     c4f10GasMPT->AddProperty("CKVRNDX", c4f10GasPhotMomCkvProd , 
                      c4f10GasCkvProdRindex,CkvProdNumPhotWaveLengthBins);

    c4f10Gas->SetMaterialPropertiesTable(c4f10GasMPT);
    RichTbc4f10Gas = c4f10Gas;


 // MirrorQuartz.
//Sio2 
//There is a quartz for the mirror and
 //another quartz which is used in aerogel and
 // yet another quartz used for the quartz window.
 //Mirrorquartz

 density=2.200*CLHEP::g/CLHEP::cm3;
 G4Material* SiO2MirrorQuartz = new G4Material(name="MirrorQuartz",
                                              density,numel=2);
 SiO2MirrorQuartz->AddElement(elSi,natoms=1);
 SiO2MirrorQuartz->AddElement(elO,natoms=2);
 
 //  G4double* MirrorQuartzRindex=new G4double[NumPhotWaveLengthBins];
  G4double* MirrorQuartzAbsorpLength=new G4double[NumPhotWaveLengthBins];
 for (G4int ibin=0; ibin<NumPhotWaveLengthBins; ibin++){
    MirrorQuartzAbsorpLength[ibin]=0.01*CLHEP::mm;
    //   MirrorQuartzRindex[ibin]=1.40;
  }
  G4MaterialPropertiesTable* MirrorQuartzMPT = 
                            new G4MaterialPropertiesTable();


  MirrorQuartzMPT->AddProperty("ABSLENGTH",PhotonMomentum,
                MirrorQuartzAbsorpLength,NumPhotWaveLengthBins);


  //  MirrorQuartzMPT->AddProperty("RINDEX", PhotonMomentum, 
  //                   MirrorQuartzRindex,NumPhotWaveLengthBins);

  SiO2MirrorQuartz->SetMaterialPropertiesTable(MirrorQuartzMPT);
  RichTbMirrorQuartz=SiO2MirrorQuartz;
 //
 // GasQuWindowQuartz.
//Sio2 

 density=2.200*CLHEP::g/CLHEP::cm3;
 G4Material* SiO2GasWinQuartz = new G4Material(name="GasWinQuartz",
                                              density,numel=2);
 SiO2GasWinQuartz->AddElement(elSi,natoms=1);
 SiO2GasWinQuartz->AddElement(elO,natoms=2);
 
 // G4int CurGasQuWinAbsorpNumBins=aRichTbGasQuWinProperty->getGasQuWinAbsorpNumBins();
 //  G4double* GasWinQuartzAbsorpLength=new G4double[CurGasQuWinAbsorpNumBins];
 //  G4double* GasWinQuAbsorpPhotonMomentum= new G4double[ CurGasQuWinAbsorpNumBins];
 //  std::vector<G4double> GasQuWinAbsorpLengthVect= aRichTbGasQuWinProperty-> getGasQuWinAbsorpLength();
 //  std::vector<G4double> GasWinQuAbsorpPhotonMomentumVect=aRichTbGasQuWinProperty->  getGasQuWinAbsorpPhMom();  
 //  for(G4int ibina=0; ibina<CurGasQuWinAbsorpNumBins; ibina++) {
   
    //    GasWinQuartzAbsorpLength[ibina]=1.E32*CLHEP::mm;
 //   GasWinQuartzAbsorpLength[ibina]=GasQuWinAbsorpLengthVect[ibina];
 //   GasWinQuAbsorpPhotonMomentum[ibina]= GasWinQuAbsorpPhotonMomentumVect[ibina];
 //  }

  // now for the refractive index of GasQuWindow.
 //  G4double* GasWinQuartzRindex=new G4double[NumPhotWaveLengthBins];
 // for (G4int ibin=0; ibin<NumPhotWaveLengthBins; ibin++){
    //    GasWinQuartzRindex[ibin]=1.40;
 //  GasWinQuartzRindex[ibin]=GasQuartzWindowNominalRefIndex;
 //  }


 //4MaterialPropertiesTable* GasWinQuartzMPT = 
 //                            new G4MaterialPropertiesTable();


 // GasWinQuartzMPT->AddProperty("ABSLENGTH",GasWinQuAbsorpPhotonMomentum,
 //               GasWinQuartzAbsorpLength,CurGasQuWinAbsorpNumBins);


 // GasWinQuartzMPT->AddProperty("RINDEX", PhotonMomentum, 
 //                    GasWinQuartzRindex,NumPhotWaveLengthBins);

 //  SiO2GasWinQuartz->SetMaterialPropertiesTable(GasWinQuartzMPT);
 // RichTbGasWinQuartz=SiO2GasWinQuartz;

//Vaccum
//
 density=CLHEP::universe_mean_density;    //from PhysicalConstants.h
 a=1.01*CLHEP::g/CLHEP::mole;
 pressure=1.e-19*CLHEP::pascal;
 temperature=0.1*CLHEP::kelvin;
// G4Material* vaccum = new G4Material(name="Galactic",z=1.,a,density,
 G4Material* vaccum = new G4Material(name="Galactic",density,numel=1,
                              kStateGas,temperature,pressure);
 vaccum->AddMaterial(Air, fractionmass=1.);
 G4double *vacAbsorpLength = new G4double[NumPhotWaveLengthBins];
 G4double *vacRindex = new G4double[NumPhotWaveLengthBins];
    for (G4int ibin = 0; ibin < NumPhotWaveLengthBins; ibin++) {
        vacAbsorpLength[ibin] = 1.E32 * CLHEP::mm;
        vacRindex[ibin] = 1.000273;
    }
    G4MaterialPropertiesTable *vacMPT = new G4MaterialPropertiesTable();

    vacMPT->AddProperty("ABSLENGTH", PhotonMomentum, 
                  vacAbsorpLength, NumPhotWaveLengthBins);

    vacMPT->AddProperty("RINDEX", PhotonMomentum, vacRindex, 
                        NumPhotWaveLengthBins);

     vaccum->SetMaterialPropertiesTable(vacMPT);
 
 RichTbVaccum=vaccum;

 // G4cout<<"Now define Aluminium "<<G4endl;
 
//Aluminium
  density=2.7*CLHEP::g/CLHEP::cm3;
  G4Material* Aluminium =new G4Material(name="Aluminium",density,numel=1);
  Aluminium->AddElement(elAL,natoms=1);
 

  G4double* AluminiumAbsorpLength= new G4double[NumPhotWaveLengthBins];

  for (G4int ibin=0; ibin<NumPhotWaveLengthBins; ibin++){
    AluminiumAbsorpLength[ibin]=0.0*CLHEP::mm;
  }


  G4MaterialPropertiesTable* AluminiumMPT = 
                            new G4MaterialPropertiesTable();


  AluminiumMPT->AddProperty("ABSLENGTH",PhotonMomentum,
                AluminiumAbsorpLength,NumPhotWaveLengthBins);



  Aluminium->SetMaterialPropertiesTable(AluminiumMPT);

  RichTbAluminium=Aluminium;

  // generic carbon
  density= 2.0*CLHEP::g/CLHEP::cm3;
  G4Material* CarbonMaterial = 
    new G4Material (name="CarbonMaterial",
                    density,numel=1 );
   CarbonMaterial->AddElement(elC, natoms=1);  
   RichTbCarbon= CarbonMaterial;
   
 
  
// Kovar
  density=2.7*CLHEP::g/CLHEP::cm3;
  G4Material* Kovar =new G4Material(PMTEnvelopeMaterialName,density,numel=1);
  Kovar->AddElement(elAL,natoms=1);
   
  PMTTubeEnvelopeMaterial=Kovar;
  HpdTubeEnvelopeMaterial=Kovar;
  
  // Silicon
  
   density=2.33*CLHEP::g/CLHEP::cm3;
   //"PMTSilicon"
  G4Material* Silicon =new G4Material(name= PMTAnodeMaterialName,
             density,numel=1);
  Silicon->AddElement(elSi,natoms=1);

  PMTAnodeMaterial=Silicon;


  G4Material* HpdSilicon =new G4Material(name= HpdSiDetMaterialName,density,numel=1);
  HpdSilicon->AddElement(elSi,natoms=1);
  HpdSiDetMaterial = HpdSilicon;

 
  // hpd quartz window

 density=2.200*CLHEP::g/CLHEP::cm3;
 //"PMTWindowQuartz"
 G4Material* PMTWindowQuartz = new G4Material(name=PmtQuartzWMaterialName,
                                              density,numel=2);
 PMTWindowQuartz->AddElement(elSi,natoms=1);
 PMTWindowQuartz->AddElement(elO,natoms=2);
 
 G4double* PMTWindowQuartzRindex=new G4double[PMTQuartzRefIndNumBins]; //31 bin
 G4double* PMTWindowQuartzPhMom = new G4double[PMTQuartzRefIndNumBins];
 G4double* PMTWindowQuartzAbsorpLength=new G4double[NumPhotWaveLengthBins];
  for (G4int ibin=0; ibin<NumPhotWaveLengthBins; ibin++){
   PMTWindowQuartzAbsorpLength[ibin]=1.E32*CLHEP::mm;
   //    PMTWindowQuartzRindex[ibin]=1.40;
  }
  for(G4int ibina=0; ibina <PMTQuartzRefIndNumBins; ibina++ ) {
     PMTWindowQuartzPhMom[ibina]=
       PhotWaveLengthToMom/
       (PMTQuartzRefWaveLenValues[ibina]*PMTQuartzRefWaveLenUnits);     
      PMTWindowQuartzRindex[ibina] = PMTQuartzRefIndexValues[ibina];
    
  }
  
  G4MaterialPropertiesTable* PMTWindowQuartzMPT = 
                            new G4MaterialPropertiesTable();

  PMTWindowQuartzMPT->AddProperty("ABSLENGTH",PhotonMomentum,
                PMTWindowQuartzAbsorpLength,NumPhotWaveLengthBins);

  PMTWindowQuartzMPT->AddProperty("RINDEX",PMTWindowQuartzPhMom , 
                        PMTWindowQuartzRindex,PMTQuartzRefIndNumBins);

  PMTWindowQuartz->SetMaterialPropertiesTable(PMTWindowQuartzMPT);

  PMTQuartzWindowMaterial=PMTWindowQuartz;

  
 //"HPDWindowQuartz"

 G4Material* HPDWindowQuartz = new G4Material(name= HpdQuartzWMaterialName,density,numel=2);
 HPDWindowQuartz->AddElement(elSi,natoms=1);
 HPDWindowQuartz->AddElement(elO,natoms=2);
 
 G4double* HPDWindowQuartzRindex=new G4double[HPDQuartzRefIndNumBins]; //31 bin
 G4double* HPDWindowQuartzPhMom = new G4double[HPDQuartzRefIndNumBins];

 G4double* HPDWindowQuartzAbsorpLength=new G4double[NumPhotWaveLengthBins];

  for (G4int ibin=0; ibin<NumPhotWaveLengthBins; ibin++){
   HPDWindowQuartzAbsorpLength[ibin]=1.E32*CLHEP::mm;
   //    PMTWindowQuartzRindex[ibin]=1.40;
  }
  for(G4int ibina=0; ibina <HPDQuartzRefIndNumBins; ibina++ ) {
     HPDWindowQuartzPhMom[ibina]=
       PhotWaveLengthToMom/
       (HPDQuartzRefWaveLenValues[ibina]*HPDQuartzRefWaveLenUnits);     
        HPDWindowQuartzRindex[ibina] = HPDQuartzRefIndexValues[ibina];
    
  }
  
  
  G4MaterialPropertiesTable* HPDWindowQuartzMPT = 
                            new G4MaterialPropertiesTable();

  HPDWindowQuartzMPT->AddProperty("ABSLENGTH",PhotonMomentum,
                HPDWindowQuartzAbsorpLength,NumPhotWaveLengthBins);

  HPDWindowQuartzMPT->AddProperty("RINDEX",HPDWindowQuartzPhMom , 
                        HPDWindowQuartzRindex,HPDQuartzRefIndNumBins);

  HPDWindowQuartz->SetMaterialPropertiesTable(HPDWindowQuartzMPT);

  HPDQuartzWindowMaterial=HPDWindowQuartz;



// crystal material

  density=2.200*CLHEP::g/CLHEP::cm3; //non e' quella giusta

 G4Material* CrystalMat = new G4Material(name=CrystalMaterialName,
                                              density,numel=2);
 CrystalMat->AddElement(elSi,natoms=1); // anche questi sono sbagliati
 CrystalMat->AddElement(elO,natoms=2);
 
  G4double* CrystalMatRindex=new G4double[CrystalMatRefIndNumBins]; //31 
  //std::vector<G4double> CrystalMatRefPhMom=Initc4f10RefPhotMom();
  G4double* CrystalMatPhMom = new G4double[CrystalMatRefIndNumBins];
  G4double* CrystalMatAbsorpLength= new G4double[CrystalMatRefIndNumBins];
  G4double* CrystalMatCkvProdRindex= new G4double[CrystalMatRefIndNumBins];
  G4cout << "n bin " << CkvProdNumPhotWaveLengthBins << G4endl;
  G4double* CrystalMatPhotMomCkvProd=
          new G4double[CrystalMatRefIndNumBins];
  //  test study for reconstruction
  //  G4bool m_activateRefIndexStudy = false;

   G4bool m_activateRefIndexStudy = true;
  
  //  G4double  CrysMatRefractiveIndexShift = m_activateRefIndexStudy ? 0.1 : 0.0 ;
  G4double  CrysMatRefractiveIndexShift = m_activateRefIndexStudy ? 0.03 : 0.0 ;
  
  // end test study for reconstruction
 
  
  RichTbAnalysisManager* aRichTbAnalysisManager = RichTbAnalysisManager::getInstance();
  
 
  for(G4int ibina=0; ibina <CrystalMatRefIndNumBins; ibina++ ) {
     CrystalMatPhMom[ibina]=
     PhotWaveLengthToMom/
    	(CrystalMatRefWaveLenValues[ibina]*CrystalMatRefWaveLenUnits);   
      CrystalMatRindex[ibina] = CrystalMatRefIndexValues[ibina] + CrysMatRefractiveIndexShift;
    //  G4cout<<"Crystal RI " << CrystalMatRindex[ibina] <<G4endl;
      //G4cout<<"momentum " << CrystalMatPhMom[ibina] <<G4endl;

      if(aRichTbAnalysisManager->getfRadiatorRefIndex()) 
        aRichTbAnalysisManager->getfRadiatorRefIndex()->Fill(CrystalMatRefWaveLenValues[ibina],
                                                             (CrystalMatRindex[ibina]-1.0) );
      
      //  CrystalMatAbsorpLength[ibina]=1.E32*CLHEP::mm;
       CrystalMatAbsorpLength[ibina] = CrystalMatAbsorptionValues[ibina]*CLHEP::mm;
       //  CrystalMatRindex[ibina]=1.5;
    // G4cout<<"Crystal RI " << CrystalMatRindex[ibin] <<G4endl;  
       //   G4cout<<"Crystal momentum and  Abs Length " <<CrystalMatPhMom[ibina]<< " "<<CrystalMatAbsorpLength[ibina] <<G4endl;
   
     G4int ibinp = ibina;
     CrystalMatCkvProdRindex[ibinp]= CrystalMatRindex[ibina];
    // G4cout << "Ckv crystal RI " << ibinp<< " " << CrystalMatCkvProdRindex[ibinp] << G4endl;
    CrystalMatPhotMomCkvProd[ibinp]=  CrystalMatPhMom[ibina];
    // G4cout << "Ckv momentum " << ibinp<< " " << CrystalMatPhotMomCkvProd[ibinp] << G4endl;
  }
  

  
  G4MaterialPropertiesTable* CrystalMPT = 
                            new G4MaterialPropertiesTable();

  CrystalMPT->AddProperty("ABSLENGTH",CrystalMatPhMom,
			  CrystalMatAbsorpLength,CrystalMatRefIndNumBins);

  CrystalMPT->AddProperty("RINDEX",CrystalMatPhMom , 
                        CrystalMatRindex,CrystalMatRefIndNumBins);

  CrystalMPT->AddProperty("CKVRNDX", CrystalMatPhotMomCkvProd , 
                      CrystalMatCkvProdRindex,CrystalMatRefIndNumBins);

  CrystalMat->SetMaterialPropertiesTable(CrystalMPT);

  CrystalMaterial=CrystalMat;


  // PMT Ph Cathode

 //the following numbers on the property of the S20 may not be accurate.
 //Some number is is jut put in for initial program test purposes.
 //The real composition is 60 nm of Na2KSb followed by 20 nm of Cs3Sb
 //from the outside to inside of the HPD. 
  density=0.100*CLHEP::g/CLHEP::cm3;
  //"S20PhCathode"


 G4Material* S20PhCathode = new G4Material(name=PmtPhCathodeMaterialName, 
                                 density, numel=4);
 S20PhCathode->AddElement(elNa, fractionmass=37.5*CLHEP::perCent);
 S20PhCathode->AddElement(elK, fractionmass=18.75*CLHEP::perCent);
 S20PhCathode->AddElement(elCs, fractionmass=18.75*CLHEP::perCent);
 S20PhCathode->AddElement(elSb, fractionmass=25.0*CLHEP::perCent);

 // G4double* S20PhCathodeRindex=new G4double[NumPhotWaveLengthBins];
  G4int  S20PhCathodeRefIndNumBins=PMTQuartzRefIndNumBins;
  G4double* S20PhCathodeRindex=new G4double[S20PhCathodeRefIndNumBins];
  G4double* S20PhCathodeRefIndPhMom = new G4double[S20PhCathodeRefIndNumBins];
 // the ref index of phcathode is derived from that of the hpd quartz window.
 // using the formula (na*na-nb*nb)/(na*na+nb*nb) = fraction of refelcted photons. 
 G4double refindphFactor=1.04;
 if (PMTQwPhReflectionProb < 1.0) 
  refindphFactor =  pow(((1+PMTQwPhReflectionProb)/(1-PMTQwPhReflectionProb)),0.5);
  for(G4int ibins=0; ibins <S20PhCathodeRefIndNumBins; ibins++ ) {
  //    S20PhCathodeRindex[ibin]=1.6;

    S20PhCathodeRefIndPhMom[ibins] =  PMTWindowQuartzPhMom[ibins];

    S20PhCathodeRindex[ibins] =   refindphFactor* PMTWindowQuartzRindex[ibins];
  }
  // now for the absorption length.
 G4double* S20PhCathodeAbsorpLength=new G4double[NumPhotWaveLengthBins];
 G4double MomBoundaryRedBlue=PhotWaveLengthToMom/PMTPhCathodeRedBlueBoundary;
 G4double ablengblue = -1.0*RichTbPMTPhCathodeThickness/log(PMTPhCathodeTransBlue);
 G4double ablengred=  -1.0*RichTbPMTPhCathodeThickness/log(PMTPhCathodeTransRed);

  for (G4int iabin=0; iabin < NumPhotWaveLengthBins; iabin++ ) {
   if(PhotonMomentum[iabin] < MomBoundaryRedBlue ) {
     S20PhCathodeAbsorpLength[iabin]=ablengred;     
   }else {
     S20PhCathodeAbsorpLength[iabin]= ablengblue;
   }
   

  }


 G4MaterialPropertiesTable* S20PhCathodeMPT = 
                            new G4MaterialPropertiesTable();
  S20PhCathodeMPT->AddProperty("ABSLENGTH",PhotonMomentum,
                S20PhCathodeAbsorpLength,NumPhotWaveLengthBins);

  //  S20PhCathodeMPT->AddProperty("RINDEX", PhotonMomentum, 
  //                      S20PhCathodeRindex,NumPhotWaveLengthBins);
  S20PhCathodeMPT->AddProperty("RINDEX",S20PhCathodeRefIndPhMom , 
                        S20PhCathodeRindex,S20PhCathodeRefIndNumBins);



  S20PhCathode->SetMaterialPropertiesTable(S20PhCathodeMPT);
  PMTPhCathodeMaterial=S20PhCathode;




  //"HPDS20PhCathode"

 G4Material* HPDS20PhCathode = new G4Material(name=HpdPhCathodeMaterialName, 
                                 density, numel=4);
 HPDS20PhCathode->AddElement(elNa, fractionmass=37.5*CLHEP::perCent);
 HPDS20PhCathode->AddElement(elK, fractionmass=18.75*CLHEP::perCent);
 HPDS20PhCathode->AddElement(elCs, fractionmass=18.75*CLHEP::perCent);
 HPDS20PhCathode->AddElement(elSb, fractionmass=25.0*CLHEP::perCent);

 // G4double* S20PhCathodeRindex=new G4double[NumPhotWaveLengthBins];

  G4int  HPDS20PhCathodeRefIndNumBins=HPDQuartzRefIndNumBins;

  G4double* HPDS20PhCathodeRindex=new G4double[HPDS20PhCathodeRefIndNumBins];
  G4double* HPDS20PhCathodeRefIndPhMom = new G4double[HPDS20PhCathodeRefIndNumBins];
 // the ref index of phcathode is derived from that of the hpd quartz window.
 // using the formula (na*na-nb*nb)/(na*na+nb*nb) = fraction of refelcted photons. 

 G4double hpdrefindphFactor=1.04;


 if (HPDQwPhReflectionProb < 1.0) 
   hpdrefindphFactor =  pow(((1+HPDQwPhReflectionProb)/(1-HPDQwPhReflectionProb)),0.5);

  for(G4int ibins=0; ibins <HPDS20PhCathodeRefIndNumBins; ibins++ ) {
  //    HPDS20PhCathodeRindex[ibin]=1.6;

    HPDS20PhCathodeRefIndPhMom[ibins] =  HPDWindowQuartzPhMom[ibins];

    HPDS20PhCathodeRindex[ibins] =   hpdrefindphFactor* HPDWindowQuartzRindex[ibins];
  }
  

  // now for the absorption length.
 G4double* HPDS20PhCathodeAbsorpLength=new G4double[NumPhotWaveLengthBins];

 G4double HpdMomBoundaryRedBlue=PhotWaveLengthToMom/HPDPhCathodeRedBlueBoundary;
 G4double hablengblue = -1.0*RichTbHpdPhCathodeThickness/log(HPDPhCathodeTransBlue);
 G4double hablengred=  -1.0*RichTbHpdPhCathodeThickness/log(HPDPhCathodeTransRed);

 for (G4int ibin=0; ibin<NumPhotWaveLengthBins; ibin++){
   //   S20PhCathodeAbsorpLength[ibin]=1.E32*CLHEP::mm;
   if(PhotonMomentum[ibin] < MomBoundaryRedBlue ) {
     // we are in the red region.
     HPDS20PhCathodeAbsorpLength[ibin]=hablengred;     
   }else {
     HPDS20PhCathodeAbsorpLength[ibin]= hablengblue;  
     
   } 

 }
 

 

  G4MaterialPropertiesTable* HPDS20PhCathodeMPT = 
                            new G4MaterialPropertiesTable();
  HPDS20PhCathodeMPT->AddProperty("ABSLENGTH",PhotonMomentum,
                                  HPDS20PhCathodeAbsorpLength,NumPhotWaveLengthBins);

  //  S20PhCathodeMPT->AddProperty("RINDEX", PhotonMomentum, 
  //                      S20PhCathodeRindex,NumPhotWaveLengthBins);

  HPDS20PhCathodeMPT->AddProperty("RINDEX",HPDS20PhCathodeRefIndPhMom , 
                                  HPDS20PhCathodeRindex,HPDS20PhCathodeRefIndNumBins);

  HPDS20PhCathode->SetMaterialPropertiesTable(HPDS20PhCathodeMPT);
  HPDPhCathodeMaterial=HPDS20PhCathode;



}





RichTbMaterial* RichTbMaterial::getRichTbMaterialInstance() 
{
  if( RichTbMaterialInstance== 0 ) {
    RichTbMaterialInstance= new RichTbMaterial();
    
  }

  return RichTbMaterialInstance;
  
}


RichTbMaterial::~RichTbMaterial()
{
    ;
}
