// This code implementation is the intellectual property of
// the GEANT4 collaboration.
//
// By copying, distributing or modifying the Program (or any work
// based on the Program) you indicate your acceptance of this statement,
// and all its terms.
//

// Make this appear first!
#include "Geant4/G4Timer.hh"

#include "RichTbRunAction.hh"

#include "Geant4/G4ios.hh"
#include "Geant4/G4Run.hh"
#include "Geant4/G4UImanager.hh"
#include "RichTbAnalysisManager.hh"
#include "RichTbCounter.hh"
#include "RichTbBeamProperty.hh"

RichTbRunAction::RichTbRunAction()
{

    timer = new G4Timer;
            ranalysisManager = RichTbAnalysisManager::getInstance();

       ranalysisManager->BeginOfRunAnalysis();

  // now for the counters

  RichTbCounter* aRichCounter =  
        RichTbCounter::getRichTbCounterInstance();

}


RichTbRunAction::~RichTbRunAction()
{
    delete timer;
}


void RichTbRunAction::BeginOfRunAction(const G4Run * aRun)
{
    G4UImanager *UI = G4UImanager::GetUIpointer();
    //   UI->ApplyCommand("/run/verbose 2");
    // UI->ApplyCommand("/event/verbose 2");
    UI->ApplyCommand("/run/verbose 1");
    UI->ApplyCommand("/event/verbose 0");
    //    UI->ApplyCommand("/tracking/verbose 0");
    // UI->ApplyCommand("/tracking/verbose 4");
    UI->ApplyCommand("/tracking/verbose 0");
    UI->ApplyCommand("/particle/process/verbose 0");
    UI->ApplyCommand("/control/verbose 0");

    G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;

    timer->Start();

    //    ranalysisManager = RichTbAnalysisManager::getInstance();

    //    ranalysisManager->BeginOfRunAnalysis();
    // initialise the beam property.

    RichTbBeamProperty* aBeamProperty= 
       RichTbBeamProperty::getRichTbBeamPropertyInstance();
    
}


void RichTbRunAction::EndOfRunAction(const G4Run * aRun)
{

      ranalysisManager = RichTbAnalysisManager::getInstance();

      ranalysisManager->EndOfRunAnalysis();

    timer->Stop();
    G4cout << "number of event = " << aRun->GetNumberOfEvent()
        << " " << *timer << G4endl;
}
