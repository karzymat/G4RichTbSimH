#include "RichTbIOData.hh"
#include "Geant4/G4HCofThisEvent.hh"
#include "Geant4/G4VHitsCollection.hh"
#include "Geant4/G4SDManager.hh"
#include "Geant4/G4ios.hh"
#include "Geant4/G4ThreeVector.hh"
#include "RichTbHit.hh"
#include <fstream>
#include "RichTbMiscNames.hh"
#include "RichTbBeamProperty.hh"

// CHANGED JDICKENS (02/06/05)
#include "RichTbUpgradeMirror.hh"
#include "RichTbGeometryParameters.hh"
// END OF CHANGE


RichTbIOData* RichTbIOData::RichTbIODataInstance=0;

RichTbIOData::RichTbIOData( )
:  OutputDataFS(((RichTbRunConfig::getRunConfigInstance())
               ->getOutputFileName()).c_str()),
  m_IsFirstEvent(true) {

  RichTbRunConfig* RConfig = RichTbRunConfig::getRunConfigInstance();

  //  OutputDataFS((RConfig->getOutputFileName()).c_str());
  aOutFileString = RConfig->getOutputFileName();

    //   const char* aOutFilechar = aOutFileString.c_str();

    //    std::ofstream  OutputDataFS(aOutFileString.c_str());

}


RichTbIOData::~RichTbIOData()
{
}


void RichTbIOData::WriteOutEventHeaderData(const G4Event * evt)
{
  RichTbRunConfig* RConfig = RichTbRunConfig::getRunConfigInstance();
  G4int curVerboseOutputLevel=  RConfig->VerboseOutputFileFlag();
  RichTbBeamProperty* aBeamProperty=
    RichTbBeamProperty::getRichTbBeamPropertyInstance();
  G4ThreeVector aBeamPos =  aBeamProperty->getBeamPosition();
  G4ThreeVector aBeamDir =  aBeamProperty-> getBeamDirection();
  G4int aRadConf = RConfig->getRadiatorConfiguration();

   // aBeamProperty->  PrintBeamProperty();

  // CHANGED JDICKENS (02/06/05)
  /*
  RichTbUpgradeMirror* aMirror = RichTbUpgradeMirror::getRichTbUpgradeMirrorInstance();
  G4ThreeVector MirrorCoC = aMirror->getMirrorCoC();
  */
  // END OF CHANGE

  G4SDManager * SDman = G4SDManager::GetSDMpointer();
  G4String colNam;
  G4int RichTbCID = SDman->GetCollectionID(colNam=RichTbHColname);
  if(RichTbCID<0) return;
  G4HCofThisEvent * HCE = evt->GetHCofThisEvent();
  RichTbHitsCollection* RHC = NULL;
  if(HCE)
  {
    RHC = (RichTbHitsCollection*)(HCE->GetHC(RichTbCID));
  }

 if(RHC)
  {
    G4int n_hit = RHC->entries();
    // G4cout << "     " << n_hit << "Hits being written out "<<G4endl;

    if(  m_IsFirstEvent ) {
      // now write the overall header word.
      // This is , for now , the verbose level.
      OutputDataFS<< curVerboseOutputLevel<< std::endl;

      //write radiator configuration (BLAGO 01/07/15)
      OutputDataFS<< aRadConf<< std::endl;


      // CHANGED TO WRITE OUT MIRROR CoC (JDICKENS 02/06/05)
      /*
      //OutputDataFS << MirrorCoC.x() << "   "
      //	   << MirrorCoC.y() << "   "
      //	   << MirrorCoC.z() << "   "
      //	   << MirrorInnerRadius << std::endl;
      G4int MirrorCode = RConfig->getMirrorOrientCode();
      G4double MirrorCoCX = MirrorPivotXInGlobalArray[MirrorCode] - MirrorInnerRadius * MirrorDirCosXArray[MirrorCode];
      G4double MirrorCoCY = MirrorPivotYInGlobalArray[MirrorCode] - MirrorInnerRadius * MirrorDirCosYArray[MirrorCode];
      G4double MirrorCoCZ = MirrorPivotZInGlobalArray[MirrorCode] - MirrorInnerRadius * MirrorDirCosZArray[MirrorCode];

      OutputDataFS << MirrorCoCX << "  "
		   << MirrorCoCY << "  "
		   << MirrorCoCZ << "  "
		   << MirrorInnerRadius << std::endl;
      */
      // END OF CHANGE

      m_IsFirstEvent=false;

    }

    if( curVerboseOutputLevel  < 1 ) {

      OutputDataFS<<n_hit<< std::endl;

    } else if ( curVerboseOutputLevel  < 3 ) {
      OutputDataFS<<n_hit<< "   "
                  << aBeamDir.x()<<"   "<<aBeamDir.y()
                  <<"   "<<aBeamDir.z()<<"   "
                  <<std::endl;
    } else if ( curVerboseOutputLevel  < 5 ) {

     OutputDataFS<<n_hit<< "   "
                << aBeamPos.x()<<"   "<<aBeamPos.y()
                << "   "<< aBeamPos.z()<<"   "
                << aBeamDir.x()<<"   "<<aBeamDir.y()
                <<"   "<<aBeamDir.z()<<"   "
                << std::endl;

    }



  }




}


void RichTbIOData::WriteOutHitData(const G4Event * evt)
{
  RichTbRunConfig* RConfig = RichTbRunConfig::getRunConfigInstance();
  G4int curVerboseOutputLevel=  RConfig->VerboseOutputFileFlag();


  G4SDManager * SDman = G4SDManager::GetSDMpointer();
  G4String colNam;
  G4int RichTbCID = SDman->GetCollectionID(colNam=RichTbHColname);
  if(RichTbCID<0) return;
  G4HCofThisEvent * HCE = evt->GetHCofThisEvent();
  RichTbHitsCollection* RHC = NULL;
  if(HCE)
  {
    RHC = (RichTbHitsCollection*)(HCE->GetHC(RichTbCID));
  }

 if(RHC)
  {
    G4int n_hit = RHC->entries();
    //    G4cout << "     " << n_hit << "Hits being written out "<<G4endl;

    for (G4int ih=0; ih<n_hit; ih++ ) {
      RichTbHit* aHit = (*RHC)[ih];
      G4int aHitPMTNum = aHit -> GetCurPMTNum();
      G4int aHitPixelNum = aHit -> GetCurPixNum();
      G4ThreeVector aHitPos = aHit -> GetPos();
      G4double aEdep =  aHit ->GetEdep() ;
      G4ThreeVector aHitLocalPos =  aHit ->GetLocalPos();
      G4ThreeVector aHitPeOrigin = aHit ->  GetPeOrigin();
      G4ThreeVector aHitPhOrigin = aHit->GetPhotonOrigin();
      G4ThreeVector  aHitPhOnQW =  aHit -> GetPhotonIncidenceOnPMTQW();
      G4double aHitCkvCosTh =   aHit ->GetCkvCosThetaProd();
      G4double aHitCkvPhi = aHit ->GetCkvPhiProd();
      G4double aHitCkvPhEner =  aHit -> GetCkvEnergyProd() ;
      G4int aHitRayleighFlag =  aHit ->GetRayleighScatFlag();
      G4int aHitMultPMTQWFlag = aHit -> GetPhotonMultIncidenceQW() ;
      G4ThreeVector aPeOriginInPhSupFrame =aHit -> GetPeOriginInPhSupport();
      G4ThreeVector aPhEmisDir =  aHit -> GetPhEmissionDir();
      G4ThreeVector aPixelLocalCenter = aHit ->  getPixelLocalCenter();
      G4ThreeVector aPixelGlobalCenter = aHit ->  getPixelGlobalCenter() ;
      G4ThreeVector aTIRCoord=  aHit -> getTIRRadiatorCoord();
      G4ThreeVector aMIRCoord=  aHit -> getMIRRadiatorCoord();
      G4ThreeVector aRFRCoord=  aHit -> getRFRRadiatorCoord();
      G4int aMultRefl = aHit ->getMultipleReflFlag();
      G4double aChTkTotMom =aHit ->getChTrackTotMom();
      G4ThreeVector aChTrackThreeMom = aHit ->getChTrackThreeMom ();



      if( curVerboseOutputLevel < 1 ) {
      OutputDataFS<<"   "<< aHitPMTNum<<"   "<<aHitPixelNum
                  <<"   "<<aHitPos.x()<<"   "<<aHitPos.y()
                  <<"   "<<aHitPos.z()<<"   "<< aEdep <<std::endl;


      }else if ( curVerboseOutputLevel < 2 ) {

      OutputDataFS<<"   "<< aHitPMTNum<<"   "<<aHitPixelNum
                  <<"   "<<aHitPos.x()<<"   "<<aHitPos.y()
                  <<"   "<<aHitPos.z()<<"   "<< aEdep
                  <<"   "<<aHitPeOrigin.x()<<"   "<<aHitPeOrigin.y()
                  <<"   "<<aHitPeOrigin.z()<<"   "<<aHitPhOnQW.x()
                  <<"   "<<aHitPhOnQW.y()  <<"   "<<aHitPhOnQW.z()
                  <<"   "<<aHitCkvCosTh    <<"   "<<aHitCkvPhEner
                  <<"   "<<aHitRayleighFlag <<std::endl;
      } else if ( curVerboseOutputLevel < 3 ) {

      OutputDataFS<<"   "<< aHitPMTNum<<"   "<<aHitPixelNum
                  <<"   "<<aHitPos.x()<<"   "<<aHitPos.y()
                  <<"   "<<aHitPos.z()<<"   "<< aEdep
                  <<"   "<<aHitPeOrigin.x()<<"   "<<aHitPeOrigin.y()
                  <<"   "<<aHitPeOrigin.z()<<"   "<<aHitPhOnQW.x()
                  <<"   "<<aHitPhOnQW.y()  <<"   "<<aHitPhOnQW.z()
                  <<"   "<<aHitCkvCosTh    <<"   "<<aHitCkvPhEner
                  <<"   "<<aHitRayleighFlag
                  <<"   "<<aPeOriginInPhSupFrame.x()
                  <<"   "<<aPeOriginInPhSupFrame.y()
                  <<"   "<<aPeOriginInPhSupFrame.z()
                  <<"    "<<aHitPhOrigin.x()
                  <<"    "<<aHitPhOrigin.y()
                  <<"    "<<aHitPhOrigin.z()
                  <<"    "<<aPhEmisDir.x()
                  <<"    "<<aPhEmisDir.y()
                  <<"    "<<aPhEmisDir.z()
                  <<"    "<<aPixelLocalCenter.x()
                  <<"    "<<aPixelLocalCenter.y()
                  <<"    "<<aPixelLocalCenter.z()
                  <<"    "<<aPixelGlobalCenter.x()
                  <<"    "<<aPixelGlobalCenter.y()
                  <<"    "<<aPixelGlobalCenter.z()
                  <<"    "<< aMultRefl
                  <<"    "<< aTIRCoord.x()
                  <<"    "<< aTIRCoord.y()
                  <<"    "<< aTIRCoord.z()
                  <<"    "<< aMIRCoord.x()
                  <<"    "<< aMIRCoord.y()
                  <<"    "<< aMIRCoord.z()
                  <<"    "<< aRFRCoord.x()
                  <<"    "<< aRFRCoord.y()
                  <<"    "<< aRFRCoord.z()
                  <<"    "<< aChTkTotMom
                  <<"    "<< aChTrackThreeMom.x()
                  <<"    "<< aChTrackThreeMom.y()
                  <<"    "<< aChTrackThreeMom.z()
                  <<std::endl;

      } else if (curVerboseOutputLevel < 5 ) {


      OutputDataFS<<"   "<< aHitPMTNum<<"   "<<aHitPixelNum
                  <<"   "<<aHitPos.x()<<"   "<<aHitPos.y()
                  <<"   "<<aHitPos.z()<<"   "<< aEdep
                  <<"   "<<aHitLocalPos.x()<<"   "<<aHitLocalPos.y()
                  <<"   "<<aHitLocalPos.z()<<"   "<<aHitPeOrigin.x()
                  <<"   "<<aHitPeOrigin.y()<<"   "<<aHitPeOrigin.z()
                  <<"   "<< aHitPhOnQW.x()<<"   "<<aHitPhOnQW.y()
                  <<"   "<< aHitPhOnQW.z()<<"   "<<aHitCkvCosTh
                  <<"   "<< aHitCkvPhi<<"   "<<aHitCkvPhEner
                  <<"   "<< aHitRayleighFlag<<"   "<<aHitMultPMTQWFlag
                  <<"    "<< aHitPhOrigin.x()<<"   "<<  aHitPhOrigin.y()
                  <<"     "<<aHitPhOrigin.z()
                  <<std::endl;
      }

    }


  }


}

RichTbIOData* RichTbIOData::getRichTbIODataInstance()
{
  if(  RichTbIODataInstance == 0) {
    RichTbIODataInstance = new RichTbIOData();

  }

  return RichTbIODataInstance;


}
