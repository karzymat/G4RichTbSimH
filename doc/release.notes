!-----------------------------------------------------------------------------
! Package     : Sim/RichTbSimH
! Responsible : Sajan Easo
! Purpose     : Simulate RICH test beam
!-----------------------------------------------------------------------------
2015-05-26 - Sajan Easo
 -Added simulation of the HPD with external readout, as used in the 2014 October testbeam.
 - The random seed kept on options file so that it can be changed for different runs
!2015-04-11 - Sajan Easo
 - Minor modifications to the histograms and sensdet for pixel number storage
!2015-03-12 -Sajan Easo
 - Added the PMT pixel Efficiency files from Paolo. This are activated on a switch and the
   RunConfig.dat is updated accordingly.
! 2015-03-10 - Roberta Cardinale
  - Updated QE and absorption tables in RichTbMaterialParameters
	- Updated RoC from 141.5 mm to 144.6 (Didier'sradius)
	- Update position of the EC
! 2015-03-09 - Sajan Easo
  - The amount of data in hit class and output data have increased. They now contain
    the coordinates of Total Internal reflection, mirror reflection and radiator exit coordinates.
    They also contain the charged track momentum
  - The pixel gap between adjacent pmt pixels implemented
! 2015-03-08 - Sajan Easo
  - The gap between radiator exit plane and pmt plane modified
  - the tiny effective air gap between radiator exit plane and dark cover reduced.
  - The mirror surace now next to the radiator without any tiny gap.
  - The user info now has more coordinates for the photon path, essentially the 
    all the coordinates where reflection and refraction happened.
  - Added a few more histograms
! 2014-11-12 - Sajan Easo , Jibo He
   - Modified a couple of input parameters: PhotonDet frame location, 
     darkcover size, size of the mirror etc.
!2014-11-06 - Sajan Easo
 - Installed the PMTS and hit creation.
 - Activated writing out the hits
! 2014-10-23 - Jibo He
 - Updated RoC to 138.4mm to 141.5mm 
   Distance between PhDet and flat surface changed to 26mm
   Put the "Ring" mirror, with radius of 39.5-58.2mm 
   DarkCover width changed to 19*2mm  
 - Changed range of histogram to store cherenkov ring, bin size 23./8.=2.875

! 2014-10-15 - Jibo He
 - Commented out many "printout"
 - src/srcmn/RichTbRunConfig.cc
   . Modified to take RunConfig.dat file from the option dir, 
     need to run "source setup.{c}sh" firstly
 - Added histograms to store reflection points on the spherical and flat surphases of Lens
 - Removed the cuts applied to calculate the cherenkov radius, modfied the range of hist

! 2014-06-27 - Sajan Easo
 - Create a first version in svn, adapted from earlier versions

