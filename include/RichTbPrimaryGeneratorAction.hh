#ifndef RichTbPrimaryGeneratorAction_h
#define RichTbPrimaryGeneratorAction_h 1

#include "Geant4/G4VUserPrimaryGeneratorAction.hh"
#include "RichTbRunConfig.hh"
#include "RichTbAnalysisManager.hh"
#include "Geant4/G4ParticleDefinition.hh"

class G4ParticleGun;
class G4Event;

class RichTbPrimaryGeneratorAction:public G4VUserPrimaryGeneratorAction {
  public:

    RichTbPrimaryGeneratorAction();
    virtual ~ RichTbPrimaryGeneratorAction();

  public:

    void GeneratePrimaries(G4Event * anEvent);
    G4ParticleDefinition*  SetParticleType();  
    void SetParticleStartPos();
    void SetParticleKineticEnergy(G4ParticleDefinition * CurPart);
    void SetParticleDirection();
  //    G4double GetCurGenPartEnergy() {
  //      return CurGenPartEnergy;
  //  }
  private:

     G4ParticleGun * particleGun;

  // G4double CurGenPartEnergy;
    G4ParticleDefinition *CurrentBeamParticle;
};
#endif                          /*RichTbPrimaryGeneratorAction_h */
