#ifndef RichTbPmtSteppingAction_h
#define RichTbPmtSteppingAction_h 1
#include "Geant4/G4UserSteppingAction.hh"
#include "Geant4/G4VDiscreteProcess.hh"
#include "Geant4/G4VParticleChange.hh"
#include "Geant4/G4ParticleChange.hh"
#include "RichTbMiscNames.hh"


class RichTbPmtSteppingAction:public  G4UserSteppingAction {

public:
  RichTbPmtSteppingAction();
  virtual ~ RichTbPmtSteppingAction();
  void UserSteppingAction(const G4Step * aStep);

private:
  G4double mChTrackMinMomFactorForHisto;


};
#endif
