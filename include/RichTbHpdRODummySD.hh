#ifndef INCLUDE_RICHTBHPDRODUMMYSD_HH 
#define INCLUDE_RICHTBHPDRODUMMYSD_HH 1

// Include files
#include "Geant4/G4VSensitiveDetector.hh"
/** @class RichTbHpdRODummySD RichTbHpdRODummySD.hh include/RichTbHpdRODummySD.hh
 *  
 *
 *  @author Sajan Easo
 *  @date   2015-05-06
 */
class RichTbHpdRODummySD: public G4VSensitiveDetector {
public: 
  /// Standard constructor
  RichTbHpdRODummySD(); 

  virtual ~RichTbHpdRODummySD(); ///< Destructor
  void Initialize(G4HCofThisEvent* /* HCE */) {}
  G4bool ProcessHits(G4Step* /* aStep */,G4TouchableHistory* /* ROhist */) {return false;}
  void EndOfEvent(G4HCofThisEvent* /* HCE */) {}
  void clear() {}
  void DrawAll() {}
  void PrintAll() {}

protected:

private:


};

RichTbHpdRODummySD::RichTbHpdRODummySD(): G4VSensitiveDetector("RichTbHpdROdummySD") {}

RichTbHpdRODummySD::~RichTbHpdRODummySD(){  }



#endif // INCLUDE_RICHTBHPDRODUMMYSD_HH
