#ifndef RichTbHall_h
#define RichTbHall_h 1

// Geant 4 headers
#include "Geant4/globals.hh"
#include "Geant4/G4VUserDetectorConstruction.hh"
#include "Geant4/G4VPhysicalVolume.hh"
#include "Geant4/G4LogicalVolume.hh"

// local headers
#include "RichTbMaterial.hh"

class RichTbHall {

  public:
    RichTbHall();
    virtual ~ RichTbHall();

    G4LogicalVolume *getRichTbHallLogicalVolume() {
        return RichTbHallLVol;
    }
    G4VPhysicalVolume *getRichTbHallPhysicalVolume() {
        return RichTbHallPVol;
    }

  private:
    G4LogicalVolume*  RichTbHallLVol;
    G4VPhysicalVolume* RichTbHallPVol;
};

#endif                          /*RichTbHall_h */
