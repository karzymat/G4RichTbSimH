#ifndef RichTbRunAction_h
#define RichTbRunAction_h 1

#include "Geant4/globals.hh"
#include "Geant4/G4UserRunAction.hh"
#include "RichTbAnalysisManager.hh"

class G4Timer;
class G4Run;

class RichTbRunAction:public G4UserRunAction {
  public:

    RichTbRunAction();
    virtual ~ RichTbRunAction();

  public:

    virtual void BeginOfRunAction(const G4Run * aRun);
    virtual void EndOfRunAction(const G4Run * aRun);
    RichTbAnalysisManager *getAnalysisMRun() {
        return ranalysisManager;
    }
  private:

     G4Timer * timer;
    RichTbAnalysisManager *ranalysisManager;
};
#endif                          /*RichTbRunAction_h */
