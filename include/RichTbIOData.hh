#ifndef RichTbIOData_h
#define RichTbIOData_h 1
#include "Geant4/globals.hh"
#include <iostream>
#include <fstream>
#include "RichTbRunConfig.hh"
#include "Geant4/G4Event.hh"
class RichTbIOData {

  public:

  //    RichTbIOData();

    virtual ~ RichTbIOData();
  static RichTbIOData* getRichTbIODataInstance();
  
    void WriteOutEventHeaderData(const G4Event *);
    void WriteOutHitData(const G4Event *);
    
  private:
    RichTbIOData();
  static RichTbIOData* RichTbIODataInstance;
  
     std::ofstream OutputDataFS;
    G4String aOutFileString;
    bool m_IsFirstEvent;
  
};
#endif
