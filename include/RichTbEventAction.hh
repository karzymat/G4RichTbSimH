#ifndef RichTbEventAction_h
#define RichTbEventAction_h 1
#include "Geant4/G4UserEventAction.hh"
#include "Geant4/G4ThreeVector.hh"

class G4Event;

class RichTbEventAction:public G4UserEventAction {

  public:
    RichTbEventAction();
    virtual ~ RichTbEventAction();
  public:
    void BeginOfEventAction(const G4Event *);
    void EndOfEventAction(const G4Event *);
    G4int GetRichCollID() {
        return RichTbCollID;
    }
    G4int GetRichCollIDHpd() {
        return RichTbCollIDHpd;
    }

  private:

    G4int RichTbCollID;
    G4int RichTbCollIDHpd;

};
#endif
