#ifndef INCLUDE_RICHTBSTEPPINGACTIONBASE_HH 
#define INCLUDE_RICHTBSTEPPINGACTIONBASE_HH 1

// Include files
#include "Geant4/G4UserSteppingAction.hh"
#include "Geant4/G4VDiscreteProcess.hh"
#include "Geant4/G4VParticleChange.hh"
#include "Geant4/G4ParticleChange.hh"
#include "Geant4/G4Step.hh"

#include "RichTbSteppingAction.hh"
#include "RichTbPmtSteppingAction.hh"

/** @class RichTbSteppingActionBase RichTbSteppingActionBase.hh include/RichTbSteppingActionBase.hh
 *  
 *
 *  @author Sajan Easo
 *  @date   2015-03-06
 */
class RichTbSteppingActionBase:public G4UserSteppingAction {
public: 
  /// Standard constructor
  RichTbSteppingActionBase( ); 

  virtual ~RichTbSteppingActionBase( ); ///< Destructor
  void UserSteppingAction(const G4Step * aStep);
  void InitRichTbStepActions();
  
protected:

private:

  RichTbSteppingAction* mRichTbSteppingAction;
  RichTbPmtSteppingAction* mRichTbPmtSteppingAction;
  

};
#endif // INCLUDE_RICHTBSTEPPINGACTIONBASE_HH
